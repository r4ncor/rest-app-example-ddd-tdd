#### How to start:

```bash
docker-compose up -d --build
```

```bash
docker-compose logs -f app
```

#### How to stop:

```bash
docker-compose down
```
